#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import dbobj
import oracledb
#import dbi
import re
import argparse
import installerapi

db_installer = installerapi.get_ora_connect('cs.bc.installer/database')
dbc_installer = db_installer.cursor()

parser = argparse.ArgumentParser(description='pytool gdbsetup.py [arguments] connectstring')
parser.add_argument('-s', '--source', help='Source for downloading', dest='source', action='store_true')
parser.add_argument('-a', '--all', help='All packages list', dest='all', action='store_true')
parser.add_argument('-p', '--part', help='Part packages list (cs.pkg.*)', dest='part', action='store_true')
parser.add_argument('connectstr')
args = parser.parse_args()

source_arg = args.source
all_arg = args.all
part_arg = args.part

#connectstr = args.connectstr
#connect= dbobj.connect(connectstr)

#### Creating directories ####


class Package:
    "This is a packages class"
    def __init__(self, id='0', parent='0', name='', typename='', raw_version=''):
        self.id = id
        self.parent = parent
        self.name = name
        self.typename = typename
        self.raw_version = raw_version
        
    version = ""
    branch = ""
    commit = ""

    def getId(self):
        return self.id
    def getParent(self):
        return self.parent
    def getName(self):
        return self.name
    def getTypeName(self):
        return self.typename
    def getRawVersion(self):
        return self.raw_version
    def getVersion(self):
        return self.version
    def getBranch(self):
        return self.branch
    def getCommit(self):
        return self.commit
    
    def setVersion(self, version):
        self.version = version
    def setBranch(self, branch):
        self.branch = branch
    def setCommit(self, commit):
        self.commit = commit
        
def identify_package(raw_package):
    if re.match("^.*_[a-z0-9]*$", str(raw_package.getRawVersion())):
        commit_number = raw_package.getRawVersion().rpartition('_')[-1]
        raw_package.setCommit(commit_number)
    else:
        if re.match("^.*modified$", str(raw_package.getRawVersion())):
            version_number = raw_package.getRawVersion().rpartition('.modified')[0]
            raw_package.setVersion(version_number) 
        else:
            raw_package.setVersion(raw_package.getRawVersion())
    
    return raw_package

def create_default_directories():
    default_source_dir = "sources"
    default_project_dir = "jgvsprojects"
    default_dist_install_dir = "jgdistinstall"
    default_install_dir = "jginstall"
    
    current_path = os.path.abspath(os.getcwd())
    path_source = os.path.join(current_path, default_project_dir, default_source_dir)
    path_jginstall = os.path.join(current_path, default_install_dir)
    path_jgdistinstall = os.path.join(current_path, default_dist_install_dir)
    
    try:
        os.mkdir(path_source)
        os.mkdir(path_jginstall)
        os.mkdir(path_jgdistinstall)
        print("Directory '%s' created" %default_dir)  
    except:
        print("Directory '%s' already exist" %default_dir) 
        pass

def get_source(all=False):
    packagesList = []
    dbc_installer.execute("select id, parent, name, typename, version, LEVEL, count(*) over (partition by name) as num_party from csinst.csinst_objects "
                                        " where LEVEL = 2 and typename = 'Package' and parent = 1 connect by PRIOR id = parent ")
    level1_packages = dbc_installer.fetchall()
    
    for level1_package in level1_packages:
        package_raw = Package(level1_package[0], level1_package[1], level1_package[2], level1_package[3], level1_package[4])
        cleaned_package = identify_package(package_raw)
        packagesList.append(cleaned_package)
        
    if all:
        for package in packagesList:
            dbc_installer.execute("select * from csinst.csinst_objects where parent = :1 and typename = 'Package'", (package.getId(),))
            level2_packages = dbc_installer.fetchall()
            for level2_package in level2_packages:
                    package_raw = Package(level2_package[0], level2_package[1], level2_package[2], level2_package[3], level2_package[4])
                    cleaned_package = identify_package(package_raw)
                    packagesList.append(cleaned_package)
                    #TODO Dublicate values in packagesList: cs.pkg.level1_appbase 1.17.2..bugfix_CSDEV_14002_094d793 AND cs.pkg.Level1_AppBase 1.17.2.4
                    #Should be only one ....094d793.....value 
    
    if len(packagesList) > 0:
        default_dir = "sources"
        current_path = os.path.abspath(os.getcwd())
        path = os.path.join(current_path, default_dir)
        
        try:
            os.mkdir(path)
            print("Directory '%s' created" %default_dir)  
        except:
            print("Directory '%s' already exist" %default_dir) 
            pass
     
    ssh_path = 'ssh://git@cvs.konts.lv:7999/te_trm/'
    
    for package in packagesList:
        try:
            ssh_path_package = ssh_path + package.getName() + '.git'
            clone_str = 'git clone ' + ssh_path_package
            
            os.chdir(path)
            os.system(clone_str)
            
            #Tag or commit
            a = os.chdir(package.getName())
            
            if package.getVersion():
                os.system('git checkout tags/' + package.getVersion())
            elif package.getCommit():
                os.system('git checkout ' + package.getCommit())
            
            #cd back   
            os.chdir(path)
            #print(ssh_path_package)
        
        except Exception as e: 
            print(e)
            pass
    
if source_arg:
    print('.....Getting source.....')
    if all_arg:
        get_source(True)
    