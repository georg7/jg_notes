#!/bin/sh

CONTROL_NAME="ri2_x23_control"
BASELINE="cs.dist.ri_ams@refs/heads/release/3.23"
RELEASE=x.23
DIST=$(echo $BASELINE| cut -d'@' -f 1)

GREEN='\033[0;32m'
NC='\033[0m'
CONTROL_PATH="/home/build/${CONTROL_NAME}"
IMAGE=localhost/home/build/${CONTROL_NAME}:${RELEASE}
THIS_SCRIPT_NAME=${0##*/}

usage() {
	echo "usage: ${THIS_SCRIPT_NAME} <cr|rm|help>"
	echo " cr        Create docker control"
	echo " rm        Stop and delete docker control"
	echo " help      Usage"
}

prepare_control() {
    CONTAINER=`container_is_up`
    if [ -d ${CONTROL_PATH} ]; then
        if [ -z ${CONTAINER} ] ; then
            echo "Try command: docker start /build_${CONTROL_NAME}_stateless"
        else
            echo "Container ${CONTAINER} already exist and started"
        fi
    else
        git clone ssh://git@cvs.konts.lv:7999/te_trm/control.git $CONTROL_NAME
        cd $CONTROL_NAME
        git checkout release/${RELEASE}
        #find . -type f -exec sed -i 's/-O2/-O0 -ggdb3/g' {} +
        docker login nexus-registry.irpc.int.tietoevry.com
        vi stateless.sh 
        ./stateless.sh start
        ./stateless.sh exec ./configure BASELINE=${BASELINE}
        echo -e "${GREEN}Get source: => make update${NC}"
        echo -e "${GREEN}Build full dist: => ./bin/dist.sh -F full -j6 cs.bc.installer ${DIST}${NC}"
        ./stateless.sh exec bash
    fi
}

container_is_up() {
	CONTAINERS=`docker ps -q -f "label=image=$IMAGE"`
	if [ -z "$CONTAINERS" ] ; then
		return 1
	fi

    COUNT=`echo $CONTAINERS|wc -w`
	eval COUNT=$COUNT
	CONTAINER=`echo $CONTAINERS|awk '{print $1}'`
	if [ $COUNT -gt 1 ] ; then
		CONTAINER=`echo $CONTAINERS|awk '{print $1}'`
	fi
	echo $CONTAINER
}

remove_control() {
    CONTAINER=`container_is_up`
    if [[ -n ${CONTAINER} ]]; then
        ./$CONTROL_NAME/stateless.sh rm
        docker network prune
        sudo rm -rf $CONTROL_NAME

        CONTAINER=`container_is_up`
        if [[ -z $CONTAINER ]]; then	
            echo -e "${GREEN}Container ${CONTROL_NAME} is removed${NC}"
        fi
    fi
}

#CONTAINER=`container_is_up`
#echo $BASELINE



for i in $@ ; do
	case $i in
	"cr" )
		shift
		prepare_control ;;
	"rm" )
		shift
		remove_control ;;
	"help" )
		shift
		usage ;;
	esac
done


