--SET SERVEROUTPUT ON  
  
DECLARE
    CURSOR terminal_id_cursor
    IS
       SELECT m.media_id FROM medias m
       WHERE m.media_type_id = '3';
       l_str   varchar2(200);
       l_del   varchar2(200);
       l_inst   varchar2(200);
       attr_val varchar2(512);
 BEGIN
    FOR i IN terminal_id_cursor
    LOOP
        BEGIN
        l_str := 'SELECT attr_value FROM media_attrs WHERE media_id = ' || i.media_id || ' AND attr_id = 98';
        execute immediate l_str into attr_val;
        
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
            CONTINUE;
        END;
        
        l_del := 'delete from media_attr_keys where attr_id=98 and attr_value=' || attr_val || ' and media_id='|| i.media_id || '';
        execute immediate l_del;
        l_inst := 'insert into media_attr_keys (attr_id, attr_value, media_id) values (98,' || attr_val || ',' || i.media_id || ')';
        execute immediate l_inst;
            
        --DBMS_OUTPUT.PUT_LINE(i.media_id || '    ' ||attr_val);

    END LOOP;
 
    COMMIT;
 END;
 
 /
 --The same with parameters:
 
  DECLARE
    CURSOR media_id_cursor
    IS
       SELECT m.media_id FROM medias m WHERE m.media_type_id IN 
       (SELECT media_type_id FROM media_types WHERE TYPECLASS IN ('POS', 'ATM', 'VIRTUAL-TERMINAL') AND state = 'OK');
       l_str   varchar2(200);
       l_upd   varchar2(200);
       attr_val varchar2(512);
       v_state VARCHAR2(20);
 BEGIN
    FOR i IN media_id_cursor
    LOOP
        BEGIN
        l_str := 'SELECT attr_value FROM media_attrs WHERE media_id = ' || i.media_id || ' AND attr_id = 100';
        execute immediate l_str into attr_val;
        
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
            CONTINUE;
        END;
        
        IF (attr_val = '0001') THEN 
            v_state := 'INSTALLED';
        ELSIF (attr_val = '0002') THEN
            v_state := 'UNINSTALLING';
        ELSIF (attr_val = '0003') THEN
            v_state := 'CLOSED';
        END IF;
        
        l_upd := 'update medias set state = :1 where media_id = '|| i.media_id ||'';
        --DBMS_OUTPUT.PUT_LINE( 'SQL: ' || l_upd); 
        execute immediate l_upd USING v_state;
        
    END LOOP;
    COMMIT;
 END;
         
/

 --TO FIND VALUE IN ANY TABLE AND IN ANY OWNER, IN EXAMPLE IN TABLES 'PARTIES%'
 --SELECT owner, table_name, column_name FROM all_tab_columns WHERE owner <> 'SYS' and data_type LIKE '%CHAR%'
 
 --SET SERVEROUTPUT ON 
 
  DECLARE
      match_count INTEGER;
      l_select   varchar2(200);
    BEGIN
      FOR t IN (SELECT owner, table_name, column_name
                  FROM all_tab_columns where owner = 'CSAPP' and table_name LIKE 'PARTIES%') LOOP

        l_select := 'SELECT COUNT(*) FROM ' || t.owner || '.' || t.table_name || ' WHERE '||t.column_name||' like ''%Book event%''';
        execute immediate l_select into match_count;
               
        IF match_count > 0 THEN
          DBMS_OUTPUT.PUT_LINE( t.table_name ||' '||t.column_name||' '||match_count );
        END IF;

      END LOOP;

    END;
    /
    
--DELETE FROM TABLES

BEGIN
  FOR c IN ( SELECT table_name FROM user_tables WHERE table_name LIKE 'DT_%' )
  LOOP
    EXECUTE IMMEDIATE 'DELETE FROM ' || c.table_name;
  END LOOP;
END;


--DELATE FROM DT_TABLES BY TABLE_ID

    
DECLARE
  table_id varchar2(200);
  l_del varchar2(200);
BEGIN
  table_id := 'tlms.state.status.CI-DEFAULT';
  FOR c IN ( SELECT table_name FROM user_tables WHERE table_name LIKE 'DT_%' )
      LOOP
        l_del := 'DELETE FROM ' || c.table_name || ' WHERE TABLE_ID = :1';
        EXECUTE IMMEDIATE l_del USING table_id;
      END LOOP;
END;

/
 