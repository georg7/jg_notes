#include <git2.h>
#include <iostream>
#include <stdio.h>
#include <string.h>

const char* pub_key;
const char* prv_key;

int cred_cb(git_cred** out, const char* url, const char* username_from_url,
	unsigned int allowed_types, void* payload) {

	std::cout << "Calling creds" << std::endl;
    const char* username = (char*)'git';

	return git_cred_ssh_key_new(out, "git",
		pub_key, prv_key, "password");
}

static void fetch_progress(
    const git_transfer_progress *stats,
    void *payload)
{
  int fetch_percent =
    (100 * stats->received_objects) /
    stats->total_objects;
  int index_percent =
    (100 * stats->indexed_objects) /
    stats->total_objects;
  int kbytes = stats->received_bytes / 1024;

  printf("network %3d%% (%4d kb, %5d/%5d)  /"
      "  index %3d%% (%5d/%5d)\n",
      fetch_percent, kbytes,
      stats->received_objects, stats->total_objects,
      index_percent,
      stats->indexed_objects, stats->total_objects);
}

static int do_clone(const char *url, const char *path, const char *pub_key, const char *prv_key)
{
  git_libgit2_init();
  git_repository *repo = NULL;
  git_clone_options clone_opts = GIT_CLONE_OPTIONS_INIT;
  clone_opts.fetch_opts.callbacks.credentials = cred_cb;
  //opts.fetch_opts.callbacks.payload = &fetch_progress;

  int ret = git_clone(&repo, url, path, &clone_opts);
  if(ret != 0){
        const git_error* err = giterr_last();
        printf("Error %d: %s\n", err->klass, err->message);
  }
  git_repository_free(repo);
  git_libgit2_shutdown();
  return ret;
}

int main(int argc, char **argv)
{
  const char *url, *path;

  if (argc < 4) {
    printf("USAGE: clone <url> <path> <path to pub key> <path to prv key>\n");
    return -1;
  }

  url = argv[1];
  path = argv[2];
  pub_key = argv[3];
  prv_key = argv[4];

  printf("URL: %s\n", url);
  printf("PATH: %s\n", path);
  printf("Path to public key: %s\n", pub_key);
  printf("Path to private key: %s\n", prv_key);

  return do_clone(url, path, pub_key, prv_key);
}