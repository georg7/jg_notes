import os.path as op

class FML_symbols(gdb.Command):
    def __init__(self):
        self.loaded = False
        self.of_path = "/home/csapp/fmlxml.o"
        super(FML_symbols, self).__init__("FML_symbols", gdb.COMMAND_USER)
        
    def print_usage(self):
        print("FML_symbols usage:")
        print(" -f <path> | --file: set path to the object file")
        print(" -h | --help: detailed help")
        print(" -l | --load: load object file")

    def print_help(self):
        print("In order to load function symbols, 2 conditions must be met:")
        print(" 1) libcsutilstux.so libary must be loaded into memory")
        print(" 2) compiled 'fmlxml.o' object file must be present in /home/csapp/ directory or specified with [-f | --file] command. After that you can use [-l | --load] param to load symbols from object file.")

    def set_ofile_path(self, path):
        if op.isfile(path):
            filename = op.basename(path)
            print("File {} is found in {}".format(filename, path))
            self.of_path = path
        else:
            print("File not found.")

    def invoke(self, args, from_tty):
        argv = gdb.string_to_argv(args)
        if not len(argv):
            self.print_usage()
            return
        else:
            param = argv[0]
            if len(param) > 1:
                if param in "-f" or param in "--file":
                    #set .o file path
                    if len(argv) < 2:
                        print("You must specify file path.")
                    else:
                        self.set_ofile_path(argv[1])
                elif param in "-l" or param in "--load":
                    #load .o file
                    self.load_file()
                elif param in "-h" or param in "--help":
                    #show help
                    self.print_help()
                else:
                    self.print_usage()
            else:
                self.print_usage()

            return

    def load_file(self):
        if self.loaded:
            print("Symbols already loaded. Step or continue to apply.")
            return
        try:
            res = gdb.execute("i func FMLtoXML", to_string=True)
            res = res.split("\"FMLtoXML\":")[1]
            if "Non-debugging symbols" not in res:
                res = gdb.execute("x FMLtoXML", to_string=True)
                if "No symbol" not in res:
                    print("Symbols already loaded.")
                else:
                    print("No FMLtoXML matches\nhint:\n - attach to the process")
                return
            if len(res.split("Non-debugging symbols:",1)) < 2:
                print("Already loaded.")
                return
            addrs = res.split("Non-debugging symbols:",1)[1].strip()
            if len(addrs) == 0:
                print("No addreses present.")
                return
            sym_1st = addrs.split("\n", 1)[0]
            addr = sym_1st.split(" ", 1)[0]
            
            gdb.execute("set confirm off")

            gdb.execute("add-symbol-file {} {} -readnow".format(self.of_path, addr))
            self.loaded = True
            print("Symbols loaded. Function will be available after continue/next step.")

            gdb.execute("set confirm on")
        except gdb.error as e:
            print("gdb.error: " + str(e))
            print("hint:\n - attach to the process")

            return

    
FML_symbols()