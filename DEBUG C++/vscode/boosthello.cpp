#include <iostream>
#include <boost/algorithm/string/predicate.hpp>

using namespace std;

int main()
{
    cout << "Hello World" << "\n";
    int idx = 22;
    std::string tagName = "COND_VALUE_22_0_0_NONBRAND";
    
    if (boost::starts_with(tagName, "COND_VALUE_") &&
        boost::ends_with(tagName, "_NONBRAND")) {

    cout << "We are in IF condition" << "\n";
        
    }

    return 0;
}